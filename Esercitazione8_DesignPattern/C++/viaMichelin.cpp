# include "viaMichelin.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

//METODI DI BUS STATION

void BusStation::Load() {
    _numberBuses = 0;
    _buses.clear();

    ifstream file;
    file.open(_busFilePath.c_str());

    if(file.fail())
        throw runtime_error("Something goes wrong");

    try {
        string line;
        getline(file,line); //Salta il commento nel file di testo
        getline(file,line);

        istringstream converter;
        converter.str(line);
        converter>> _numberBuses;

        getline(file,line); //Salta il commento nel file di testo
        getline(file,line); //Salta il commento nel file di testo

        _buses.reserve(_numberBuses);
        for (unsigned int b = 0; b< (unsigned int)_numberBuses; b++)
        {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _buses[b].Id >> _buses[b].FuelCost;
        }
    }

    catch (exception) {
        _numberBuses = 0;
        _buses.clear();

        throw runtime_error("Something goes wrong");
    }

    file.close();
}

const Bus &BusStation::GetBus(const int &idBus) const {
    if (idBus > _numberBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses [idBus-1];
}

//METODI DI MAP DATA

void MapData::Reset()
{
  _numberRoutes = 0;
  _numberStreets = 0;
  _numberBusStops = 0;
  _busStops.clear();
  _streets.clear();
  _routes.clear();
  _streetsFrom.clear();
  _streetsTo.clear();
  _routeStreets.clear();
}

void MapData::Load() {
    /// Reset map
    Reset();

    /// Open File
    ifstream file;
    file.open(_mapFilePath.c_str());

    if (file.fail())
      throw runtime_error("Something goes wrong");

    /// Load
   try{
    string line;

    /// Get busStops
    getline(file, line); // Skip Comment Line
    getline(file, line);
    istringstream convertBusStops;
    convertBusStops.str(line);
    convertBusStops >> _numberBusStops;

    getline(file, line); // Skip Comment Line
    _busStops.resize(_numberBusStops);
    for (int b = 0; b < _numberBusStops; b++)
    {
      getline(file, line);
      istringstream converter;
      converter.str(line);
      converter >> _busStops[b].Id >> _busStops[b].Name >> _busStops[b].Latitude >> _busStops[b].Longitude;
    }

    /// Get streets
    getline(file, line); // Skip Comment Line
    getline(file, line);
    istringstream convertStreets;
    convertStreets.str(line);
    convertStreets >> _numberStreets;

    getline(file, line); // Skip Comment Line
    _streets.resize(_numberStreets);
    _streetsFrom.resize(_numberStreets);
    _streetsTo.resize(_numberStreets);
    for (int s = 0; s < _numberStreets; s++)
    {
      getline(file, line);
      istringstream converter;
      converter.str(line);
      converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
    }

    /// Get routes
    getline(file, line); // Skip Comment Line
    getline(file, line);
    istringstream convertRoutes;
    convertRoutes.str(line);
    convertRoutes >> _numberRoutes;

    getline(file, line); // Skip Comment Line
    _routes.resize(_numberRoutes);
    _routeStreets.resize(_numberRoutes);
    for (int r = 0; r < _numberRoutes; r++)
    {
      getline(file, line);
      istringstream converter;
      converter.str(line);
      converter >> _routes[r].Id >> _routes[r].NumberStreets;
      _routeStreets[r].resize(_routes[r].NumberStreets);

      for (int s = 0; s < _routes[r].NumberStreets; s++)
        converter >> _routeStreets[r][s]; // salva le strade percorse nell'intero circuito (è un vettore di vettori routeStreets).
    }

    /// Close File
    file.close();

    }
    catch (exception) {
    Reset();

    throw runtime_error("Something goes wrong");
  }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if (idRoute > _numberRoutes)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
      throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}
const Route &MapData::GetRoute(const int &idRoute) const {
    if (idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idFrom = _streetsFrom[idStreet - 1];

    return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idTo = _streetsTo[idStreet - 1];

    return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if (idBusStop > _numberBusStops)
      throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

//METODI DI ROUTE PLANNER

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++)
      travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;


    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {
    int routeCost = 0;
    double t,benzina;
    t = ComputeRouteTravelTime(idRoute);
    benzina = _busStation.GetBus(idBus).FuelCost;

    //Calcolo del costo dell'intero percorso. (
    //Bisogna dividere per 3600, in quanto il tempo totale è calcolato in secondi e va convertito in ore, poichè la benzina è in euro/km e la velocità è data in km/h
    routeCost = t * benzina * BusAverageSpeed / 3600;
    return routeCost;
}

//METODI DI MAP VIEWER

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int s = 0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; s < route.NumberStreets - 1; s++)
    {
      int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      routeView << from<< " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
