#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point Point::operator+(const Point& point) const
{
    Point tempPoint (X,Y);
    tempPoint.X = X + point.X;
    tempPoint.Y = Y + point.Y;
    return tempPoint;
}

Point Point::operator-(const Point& point) const
{
    Point temp(X,Y);
    temp.X = X - point.X;
    temp.Y = Y - point.Y;
    return temp;
}

Point&Point::operator-=(const Point& point)
{
    X -= point.X;
    Y -= point.Y;
    return *this;
}

Point&Point::operator+=(const Point& point)
{
    X += point.X;
    Y += point.Y;
    return *this;
}

Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Perimeter() const
  {
    double perimetro;
    perimetro = 2*M_PI* sqrt( (_a*_a + _b*_b)/2 );
    return perimetro;
  }

// Per il perimetro del cerchio si sfrutta l'implementazione dell'ellisse, sapendo che a = b = raggio
Circle::Circle(const Point &center, const double &radius)  : Ellipse (center,radius,radius) { }

double Circle::Perimeter() const { return Ellipse :: Perimeter(); }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
     AddVertex(p1);
     AddVertex(p2);
     AddVertex(p3);
  }

  double Triangle::Perimeter() const
  {
    double l1,l2,l3, perimeter = 0;
    // Calcolo la lunghezza dei tre lati usando la formula della distanza tra due punti.
    l1 = sqrt( abs(points[0].X - points[1].X)* abs(points[0].X - points[1].X) + abs(points[0].Y - points[1].Y)*abs(points[0].Y - points[1].Y) );
    l2 = sqrt( (points[1].X - points[2].X)*(points[1].X - points[2].X) + (points[1].Y - points[2].Y)*(points[1].Y - points[2].Y) );
    l3 = sqrt( abs(points[0].X - points[2].X)* abs(points[0].X - points[2].X) + (points[0].Y - points[2].Y)*(points[0].Y - points[2].Y) );
    perimeter = l1 + l2 + l3;

    return perimeter;
  }

  // Per il calcolo del perimetro del triangolo equilatero si sfrutta l'implementazione del triangolo generale, sapendo un punto e ricavando gli altri due punti usando il lato
  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge) : Triangle ( p1, Point((p1.X + 0.5*edge),(p1.Y + sqrt(3)/2*edge)), Point( (p1.X-0.5*edge) , (p1.Y + sqrt(3)/2*edge)) )
  { }

  double TriangleEquilateral::Perimeter() const { return Triangle :: Perimeter(); }


  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
      AddVertex(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0, l1,l2,l3,l4;
    // La lunghezza dei 4 lati viene calcolata usando la formula della distanza tra due punti
    l1 = sqrt( abs(points[0].X - points[1].X)* abs(points[0].X - points[1].X) + abs(points[0].Y - points[1].Y)*abs(points[0].Y - points[1].Y) );
    l2 = sqrt( (points[1].X - points[2].X)*(points[1].X - points[2].X) + (points[1].Y - points[2].Y)*(points[1].Y - points[2].Y) );
    l3 = sqrt( (points[2].X - points[3].X)*(points[2].X - points[3].X) + (points[2].Y - points[3].Y)*(points[2].Y - points[3].Y) );
    l4 = sqrt( abs(points[0].X - points[3].X)* abs(points[0].X - points[3].X) + abs(points[0].Y - points[3].Y)*abs(points[0].Y - points[3].Y) );

    perimeter = l1 + l2 + l3 + l4;
    return perimeter;
  }
// Il perimetro del rettangolo si calcola, sapendo un punto, passato da parametro, e ricavando gli altri tre punti sapendo base e altezza
  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) : Quadrilateral ( p1, Point( (p1.X+base), p1.Y) , Point( (p1.X+base) , (p1.Y+height) ), Point( p1.X, (p1.Y+height) ) )
  { }

  double Rectangle::Perimeter() const { return Quadrilateral :: Perimeter(); }

  //Il perimetro del quadrato si calcola sfruttando l'implementazione del rettangolo, sapendo che base = altezza = lato
  Square::Square(const Point &p1, const double &edge) : Rectangle (p1 , edge, edge)
  { }

  double Square::Perimeter() const { return Rectangle ::Perimeter();}



}
