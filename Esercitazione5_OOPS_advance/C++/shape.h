#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
#include <cmath>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X=0.0; Y=0.0; } // costruttore vuoto
      Point(const double& x,
            const double& y) {X = x;
                              Y = y;} //creo il punto utilizzando i valori di x e y passati da paramero
      Point(const Point& point) { X = point.X; Y = point.Y ; } // costruttore copia

      double ComputeNorm2() const { return 0; }

      // Operatori
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " << point.X << "y = " << point.Y << endl ;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;

      // operatori dei poligoni
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() < lhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() <= lhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { }
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { }
      Circle(const Point& center,
             const double& radius);
      double Perimeter() const;
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      // I punti passati da parametro vengono salvati in un vettore di punti
      vector<Point> points;

    public:
      Triangle()
      { // La funzione 'reserve' predispone nella memoria uno spazio per gli elemeti del vettore, pur non riempendo il vettore e non dando ad esso una dimensione predefinita
        // Il vettore viene qui utilizzato come una lista

          points.reserve(3);
      }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      // Questa funzione serve ad aggiungere i punti passati da parametro al vettore di punti
      void AddVertex(const Point& point) { points.push_back(point);}
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
    public:
      double Perimeter() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral()
      { // Viene lasciato spazio in memoria per 4 elementi del vettore
          points.reserve(4); }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      // Questo metodo serve ad aggiungere al vettore di punti quelli passati da parametro
      void AddVertex(const Point& p) { points.push_back(p); }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      double Perimeter() const;
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {}
      Square(const Point& p1,
             const double& edge);
      double Perimeter() const;
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
