from unittest import TestCase

import main as pizzaLibrary


class TestPizzeria(TestCase):
    def test_ingredient(self):
        pizzeria = pizzaLibrary.Pizzeria()

        try:
            pizzeria.addIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.addIngredient("Mozzarella", "Traditionally southern Italian cheese", 3)
        except Exception as ex:
            self.fail()

        try:
            pizzeria.addIngredient("Mozzarella", "Traditionally southern Italian cheese", 3)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Ingredient already inserted")

        try:
            ingredient = pizzeria.findIngredient("Mozzarella")
            self.assertEqual(ingredient.Name, "Mozzarella")
            self.assertEqual(ingredient.Description, "Traditionally southern Italian cheese")
            self.assertEqual(ingredient.Price, 3)
        except Exception as ex:
            self.fail()

        try:
            ingredient = pizzeria.findIngredient("Eggplant")
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Ingredient not found")

        try:
            self.assertEqual(pizzeria.listIngredients(),
                             "Mozzarella - 'Traditionally southern Italian cheese': 3 euro\nTomato - 'Red berry of "
                             "the plant Solanum lycopersicum': 2 euro\n")
        except Exception as ex:
            self.fail()

    def test_pizza(self):
        pizzeria = pizzaLibrary.Pizzeria()

        try:
            pizzeria.addIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.addIngredient("Mozzarella", "Traditionally southern Italian cheese", 3)

            pizzeria.addPizza("Margherita", ["Tomato", "Mozzarella"])
            pizzeria.addPizza("Marinara", ["Tomato"])
        except Exception as ex:
            self.fail()

        try:
            pizzeria.addPizza("Margherita", ["Tomato", "Mozzarella"])
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Pizza already inserted")

        try:
            pizza = pizzeria.findPizza("Margherita")
            self.assertEqual(pizza.Name, "Margherita")
            self.assertEqual(pizza.numIngredients(), 2)
            self.assertEqual(pizza.computePrice(), 5)
        except Exception as ex:
            self.fail()

        try:
            pizza = pizzeria.findPizza("Prosciutto")
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Pizza not found")

        try:
            self.assertEqual(pizzeria.menu, "Margherita (2 ingredients): 5 euro\nMarinara (1 ingredients): 2 euro\n")
        except Exception as ex:
            self.fail()


    def test_order(self):
        pizzeria = pizzaLibrary.Pizzeria()

        try:
            pizzeria.addIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.addIngredient("Mozzarella", "Traditionally southern Italian cheese", 3)

            pizzeria.addPizza("Margherita", ["Tomato", "Mozzarella"])
            pizzeria.addPizza("Marinara", ["Tomato"])

            self.assertEqual(pizzeria.createOrder(["Margherita", "Marinara"]), 1000)
        except Exception as ex:
            self.fail()

        try:
            pizzeria.createOrder({})
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Empty order")

        try:
            order = pizzeria.findOrder(1000)
            self.assertEqual(order.numPizzas(), 2)
            self.assertEqual(order.computeTotal(), 7)
        except Exception as ex:
            self.fail()

        try:
            order = pizzeria.findOrder(1001)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Order not found")

        try:
            order = pizzeria.getReceipt(1001)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Order not found")

        try:
            self.assertTrue(pizzeria.getReceipt(1000) == "- Margherita, 5 euro\n- Marinara, 2 euro\n  TOTAL: 7 euro\n")
        except Exception as ex:
            self.fail()
