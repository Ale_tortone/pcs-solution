class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredienti = []

    def addIngredient(self, ingredient: Ingredient):
        self.ingredienti.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredienti)

    def computePrice(self) -> int:
        prezzo = 0

        # itero sul vettore ingredienti per salvare in 'prezzo' la somma del costo di ogni ingrediente della pizza
        for i in range(0, self.numIngredients()):
            prezzo = prezzo + self.ingredienti[i].Price

        return prezzo


class Order:
    def __init__(self):
        self.ordinePizze = []

    def getPizza(self, position: int) -> Pizza:
        return self.ordinePizze[position]

    def initializeOrder(self, numPizzas: int):
        pass

    def addPizzas(self, pizza: Pizza):
        self.ordinePizze.append(pizza)

    def numPizzas(self) -> int:
        return len(self.ordinePizze)

    def computeTotal(self) -> int:
        totale = 0
        for i in range(0, self.numPizzas()):
            totale = totale + self.ordinePizze[i].computePrice
        return totale


class Pizzeria:
    def __init__(self):
        self.elencoIngredienti = []
        self.elencoPizze = []
        self.elencoOrdini = []

    def addIngredient(self, name: str, description: str, price: int):
        found = False
        for i in range(0, len(self.elencoIngredienti)):
            if name == self.elencoIngredienti[i].Name:  # Non so se ci vada il '.Name'
                found = True
                break
        if found == True:
            raise Exception("Ingredient already inserted")
        else:
            ingrediente = Ingredient(name, price, description)
            self.elencoIngredienti.append(ingrediente)

    def findIngredient(self, name: str) -> Ingredient:
        posizione = 0
        found = False
        for i in range(0, len(self.elencoIngredienti)):
            if name == self.elencoIngredienti[i].Name:  # Non so se ci vada il '.Name'
                found = True
                posizione = i
                break

        if found == True:
            return self.elencoIngredienti[posizione]
        else:
            raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        found = False
        for i in range(0, len(self.elencoPizze)):
            if name == self.elencoPizze[i].Name:  # Non so se ci vada il '.Name'
                found = True
                break
        if found == True:
            raise Exception("Pizza already inserted")
        else:
            _pizza = Pizza(name)
            for i in range(0, len(ingredients)):
                _ingrediente = self.findIngredient(ingredients[i])
                _pizza.addIngredient(_ingrediente)

            self.elencoPizze.append(_pizza)

    def findPizza(self, name: str) -> Pizza:
        posizione = 0
        found = False
        for i in range(0, len(self.elencoPizze)):
            if name == self.elencoPizze[i].Name:  # Non so se ci vada il '.Name'
                found = True
                posizione = i
                break

        if found == True:
            return self.elencoPizze[posizione]
        else:
            raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        sizeVector = len(pizzas)
        _ordine = Order
        _pizza = Pizza

        if sizeVector == 0:
            raise ValueError("Empty order")
        else:
            for i in range(0, sizeVector):
                _pizza = self.findPizza(pizzas[i])
                _ordine.addPizzas(_pizza)

            self.elencoOrdini.append(_ordine)

            sizeElenco = len(self.elencoOrdini)
            return (sizeElenco - 1 + 1000)

    def findOrder(self, numOrder: int) -> Order:
        posizione = numOrder - 1000
        if posizione < 0 or posizione > (len(self.elencoOrdini) - 1):
            raise ValueError("Order not found")
        else:
            return self.elencoOrdini[posizione]

    def getReceipt(self, numOrder: int) -> str:
        total = 0
        size = len(self.elencoOrdini)
        posizione = numOrder - 1000
        # stringa

        if posizione < 0 or posizione > (size - 1):
            raise ValueError("Order not found")
        else:
            for i in range(0, self.elencoOrdini[posizione].numPizzas):
                total = total + self.elencoOrdini[posizione].GetPizza(i).computePrice()
                # stringa = stringa + "- " + self.elencoOrdini[posizione].GetPizza(j).Name + ", " + self.elencoOrdini[posizione].GetPizza(j).ComputePrice() + " euro" + "\n"

    def listIngredients(self) -> str:
        size = len(self.elencoIngredienti)
        # self.elencoIngredienti.sort()

        temp = Ingredient
        temp = self.elencoIngredienti[0]
        name = temp.Name
        description = temp.Description
        price = str(temp.Price)

        string = name + " - " + description + " : " + price + " euro\n"


        for i in range(1, size):
            temp = self.elencoIngredienti[i]
            name = temp.Name
            description = temp.Description
            price = str(temp.Price)

            string = string + name + " - " + description + " : " + price + " euro\n"


        return string

    def menu(self) -> str:
        temp = Pizza
        temp = self.elencoPizze[0]
        name = temp.Name
        numIngredients = str(temp.numIngredients)
        price = str(temp.computePrice)

        string = name + " (" + numIngredients + ") " + price + " euro\n"

        for i in range(1, size):
            temp = self.elencoPizze[i]
            name = temp.Name
            numIngredients = str(temp.numIngredients)
            price = str(temp.computePrice)

            string = string + name + " (" + numIngredients + ") " + price + " euro\n"

        return string
