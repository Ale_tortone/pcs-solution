#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <string.h>
#include <algorithm>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
    public:
      string Name;
      vector<Ingredient> ingredienti;

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {

    public:
      vector<Pizza> ordinePizze;

    public:
      void InitializeOrder(int numPizzas) { throw runtime_error("Unimplemented method"); }
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const { return ordinePizze.size(); }
      int ComputeTotal() const ;
  };


  class Pizzeria {
    public:
      vector<Ingredient> elencoIngredienti;
      vector<Pizza> elencoPizze;
      vector<Order> elencoOrdini;

    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);


      const Ingredient& FindIngredient(const string& name) const;

      void AddPizza(const string& name,
                    const vector<string>& ingredients);


      const Pizza& FindPizza(const string& name) const;

      int CreateOrder(const vector<string>& pizzas);

      const Order& FindOrder(const int& numOrder) const;

      string GetReceipt(const int& numOrder) const;

      string ListIngredients()
      {
          int i, size, cfr;
          size = elencoIngredienti.size();
          string stringa;
          // sort (elencoIngredienti.begin() , elencoIngredienti.end(), cstr_compare);

          Ingredient val_sist,temp;

          int j, min;

        /*  Ho provato a fare l'ordinamento ma non mi viene
          Ho tolto il const dall'attributo per modificare elencoIngredienti. puoi provare poi a creare un altro elenco di ingredienti
          non modificando quindi l'elenco originario, ma creandone uno nuovo ordinato. */

          for (i = 0; i < size - 1; i++)
          {
              min = i;
              for (j = i + 1; j < size; j++)
              {
                  if ( (cfr= (strcmp(elencoIngredienti[j].Name.c_str() , elencoIngredienti[i +1].Name.c_str()) )) <0 )
                  min = j;
              }

           // Scambia i due elementi
              temp = elencoIngredienti[i];
              elencoIngredienti[min] = elencoIngredienti[i];
              elencoIngredienti[i] = temp;
          }

          for(i=0; i< size; i++)
          {
              stringa = stringa + elencoIngredienti[i].Name + " - '" + elencoIngredienti[i].Description + "': " + to_string(elencoIngredienti[i].Price) + " euro" + "\n";
          }

          return stringa;

      }

      string Menu()
      {
          // Il metodo implementato sotto nel caso del menù funziona mentre con gli ingredienti no. Non so perchè.

          unsigned int i,j,size,min;
          int cfr;
          size = elencoPizze.size();
          string stringa;
          Pizza temp;

          // Metodo alternativo con sort che non so come funzioni
          // sort ( elencoPizze.begin()->Name, elencoPizze.end()->Name);

          for (i = 0; i < size - 1; i++)
          {
              min = i;
              for (j = i + 1; j < size; j++)
              {
                  if ( (cfr= (strcmp(elencoPizze[j].Name.c_str() , elencoPizze[i +1].Name.c_str()) )) < 0 )
                  min = j;
              }

           // Scambia i due elementi
              temp = elencoPizze[i];
              elencoPizze[min] = elencoPizze[i];
              elencoPizze[i] = temp;
          }

          for(i=0; i< elencoPizze.size(); i++)
          {
             stringa = stringa + elencoPizze[i].Name + " (" + to_string(elencoPizze[i].NumIngredients() )+ " ingredients): " + to_string(elencoPizze[i].ComputePrice() ) + " euro" + "\n";
          }

          return stringa;
      }
  };
};

#endif // PIZZERIA_H
