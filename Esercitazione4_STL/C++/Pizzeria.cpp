#include "Pizzeria.h"

namespace PizzeriaLibrary {

// METODI DELLA CLASSE PIZZA
void Pizza::AddIngredient(const Ingredient &ingredient) {ingredienti.push_back(ingredient);}

int Pizza::NumIngredients() const { return ingredienti.size(); }

//salvo in 'prezzo' la somma del costo di ogni ingrediente con cui è fatta la pizza.
int Pizza::ComputePrice() const
{
    int prezzo=0;

    // Utilizzo l'iteratore it per scorrere il vettore di ingredienti.
    for ( vector<Ingredient>::const_iterator it = ingredienti.begin(); it != ingredienti.end(); it++)
    {
        prezzo = prezzo + it->Price;
    }
    return prezzo;
}


// METODI DELLA CLASSE ORDER
void Order::AddPizza(const Pizza &pizza) { ordinePizze.push_back(pizza); }

// Data la posizione, questo metodo mi restituisce la pizza corrsipondente a tale posizione, all'interno dell'ordine corrente
const Pizza &Order::GetPizza(const int &position) const { return ordinePizze[position]; }

// Viene salvata in 'totale' la somma di ogni pizza, facente parte dell'ordine.
int Order::ComputeTotal() const
{
    int totale=0,i;
    for (i=0; i< int(ordinePizze.size() ); i++)
    {
        totale = totale + ordinePizze[i].ComputePrice();
    }
    return totale;
}


// METODI DELLA CLASSE PIZZERIA

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    // Si controlla che l'ingrediente non sia già presente nell'elenco, quindi si procede all'inserimento.
    // Se questo è già presente, viene lanciata un'eccezione.

    bool found = false;

    for ( vector<Ingredient>::iterator it = elencoIngredienti.begin(); it != elencoIngredienti.end(); it++)
    {
        if ( name == it->Name)
        {
            found = true;
            break;
        }
    }

    if(found == true)
        throw runtime_error("Ingredient already inserted");

    else
    {
        Ingredient _ingredient;
        _ingredient.Name = name;
        _ingredient.Description = description;
        _ingredient.Price = price;

        elencoIngredienti.push_back(_ingredient);

    }
}

// Questo metodo serve a individuare un ingrediente nel vettore di ingredienti, passando da paramentro il nome di esso.
const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    // Viene utilizzato un iteratore per scorrere l'elenco di ingredienti. Se l'ingrediente esiste, viene restituito l'oggetto ingrediente, corrispondente al nome passato da paramento
    // altrimenti viene lanciata un'eccezione.
      int posizione=0,i=0;
      bool found=false;
      for ( vector<Ingredient>::const_iterator itr = elencoIngredienti.begin(); itr != elencoIngredienti.end(); itr++)
      {
           if(itr->Name == name)
           {
               found = true;
               posizione = i;
               break;
           }
           i++;
       }

       if(found == true)
            return elencoIngredienti[posizione];
       else
           throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    // Si controlla innanzitutto che la pizza non sia già presente nel menù, quindi si procede all'inserimento

    bool found = false;
    for( vector<Pizza>::iterator it = elencoPizze.begin(); it != elencoPizze.end(); it++)
    {
        if(it ->Name == name)
        {
            found = true;
            break;
        }
    }
    if(found == true)
        throw runtime_error("Pizza already inserted");
    else
    {
        // Viene creata una nuova pizza al cui interno vengono salvati gli ingredienti, passati da parametro come componenti di un vettore
        Pizza _pizza;
        _pizza.Name = name;
        Ingredient _ingrediente;

        // Utilizzo 'FindIngredient' per trovare l'ingrediente che corrsiponde al nome passato da parametro e lo faccio per tutto il vettore di stringhe ingredients
        for(int i=0; i< int (ingredients.size() ); i++)
        {
            _ingrediente = FindIngredient(ingredients[i]);

            _pizza.AddIngredient(_ingrediente);
        }

        // La pizza creata viene quindi aggiunta al menù

        elencoPizze.push_back(_pizza);
    }
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    // Viene utilizzato un iteratore per scorrere l'elenco di pizze. Se la pizza esiste, viene restituito l'oggetto pizza, corrispondente al nome passato da paramento
    // altrimenti viene lanciata un'eccezione.
      int posizione=0,i=0;
      bool found=false;
      for ( vector<Pizza>::const_iterator itr = elencoPizze.begin(); itr != elencoPizze.end(); itr++)
      {
           if(itr->Name == name)
           {
               found = true;
               posizione = i;
               break;
           }
           i++;
       }

       if(found == true)
            return elencoPizze[posizione];
       else
           throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    Order _ordine;
    Pizza _pizza;
    int sizeVector = pizzas.size();

    // Viene lanciata un'eccezione nel caso in cui l'ordine sia vuoto, altrimenti si procede all'inseriemento delle pizze nell'ordine
    if(sizeVector==0)
        throw runtime_error("Empty order");

    else
    {
        // Utilizzo 'FindPizza' per trovare la pizza che corrsiponde al nome passato da parametro e lo faccio per tutto il vettore di stringhe 'pizzas'
        for(int i=0; i<sizeVector; i++)
        {
            _pizza = FindPizza(pizzas[i]);
            _ordine.AddPizza(_pizza);
        }

        elencoOrdini.push_back(_ordine);

        // Il metodo deve restituire un intero che corrsiponde al numero dell'ordine, considerando che al primo ordine deve corrispondere il numero 1000
        int sizeElenco = elencoOrdini.size();
        return sizeElenco - 1 +1000;
    }


}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    // Viene individuato nell'elenco di ordini, quello corrispondente al numero fornito da parametro.
    // Se il numero dell'ordine risulta essere fuori dal range di ordini salvati, viene restituita un'eccezione.
    int posizione;
    posizione = numOrder-1000;

    if(posizione<0 || posizione > int (elencoOrdini.size())-1 )
        throw runtime_error("Order not found");
    else
        return elencoOrdini[posizione] ;
}

// Questo metodo restituisce lo scontrino dell'ordine corrsispondente al numero fornito da parametro
string Pizzeria::GetReceipt(const int &numOrder) const
{
    int posizione,size,j,total=0;
    size = elencoOrdini.size();
    posizione = numOrder-1000;
    string stringa;

    // Viene lanciata un'eccezione nel caso in cui l'ordine non sia presente
    if(posizione<0 || posizione > size-1 )
        throw runtime_error("Order not found");
    else
    {
        // devo inserire un ciclo for, per scorrere le pizze all'interno dell'ordine e salvare in 'total' il totale dello scontrino.
        for(j=0 ; j< elencoOrdini[posizione].NumPizzas() ;j++)
        {
            total = total + elencoOrdini[posizione].GetPizza(j).ComputePrice();
            stringa = stringa + "- " + elencoOrdini[posizione].GetPizza(j).Name + ", " + to_string(elencoOrdini[posizione].GetPizza(j).ComputePrice() ) + " euro" + "\n";
        }

        //Il contenuto della stringa (in particolare l'elenco delle pizze), è stato salvato nella stringa, che ora viene restituita insieme al conto totale.
        return stringa +  "  TOTAL: " + to_string(total) + " euro" + "\n";
    }
}




}
