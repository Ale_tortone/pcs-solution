import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    text = ""
    file = open(inputFilePath, "r")
    line = file.readlines()
    stringaLine = str(line)
    for i in range (2, len(stringaLine)-4):
        text = text + stringaLine[i]

    # text = str(line)

    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt (text, password):
    i = 0
    encryptedText = ""
    for i in range(0, len(text)):
        textAscii = ord(text[i]) + ord(password[i % len(password)])
        # \il carattere viene convertito da codice ascii nuovamente in char
        carattere = chr(textAscii)
        encryptedText= encryptedText + carattere


    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    i = 0
    decryptedText = ""
    for i in range(0, len(text)):
        textAscii = ord(text[i]) - ord(password[i % len(password)])
        carattere = chr(textAscii)
        decryptedText = decryptedText + carattere

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]
    control=0
    j=0
    for j in range (0,len(password)):
        n = ord(password[j])
        if n<65 or n>90 :
            control = 1
    if control==1:
        print ("La password inserita non è formata da caratteri maiuscoli")
        exit (-1)
    print(password)

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)


    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
