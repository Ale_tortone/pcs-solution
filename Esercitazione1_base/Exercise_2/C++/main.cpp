#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <string>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  char a,b,c;
  int d,e;
  a = char(100);
  b = char (230);
  c = char(180);
  cout<<a<< endl;
  cout<< b<< endl;
  cout<< c<< endl;

  d = int('d');
  e = int('?');


  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
   {
     cout<< "Encryption successful: result= ";

     // Ho provato questo modo per risolvere il problema che non mi stampa il testo criptato, ma non funziona comunque
     int i,len;
     len = encryptedText.length();
     for(i=0;i<(len);i++)
     {
         cout<< encryptedText[i];
     }
     cout<< endl;
   }


  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}



bool ImportText(const string& inputFilePath,
                string& text)
{
  ifstream file;
  file.open (inputFilePath);

  getline(file,text);

  file.close();

  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{int i=0,size=0,lenText=0;

    while(password[size]!='\0')
        size++;

    while(text[lenText]!='\0')
        lenText++;

    cout<< password<< endl;
    cout<< size<< endl;

    // In "asciiP" viene salvata la password convertita in codice ascii, in "v" il testo convertito in ascii, e in "asciiEn" il testo criptato, in codice ascii.
    int asciiP[size+1],v[lenText],asciiEn[lenText];

    // Il seguente ciclo for serve per convertire la password in codice ascii
    for(i=0;i<size;i++)
        asciiP[i]=int(password[i]);
    for(i=0;i<size;i++)
        cout<< "ap" << asciiP[i]<< endl; // qua la stampa giusta
    cout<< "password intera "<< asciiP<< endl; // viene stampata sbagliata

    // Il seguente ciclo for serve per convertire il testo da criptare in codice ascii
    for(i=0;i<lenText;i++)
        v[i]=int(text[i]);

    for(i=0;i<lenText;i++)
        cout<< "tdc"<< v[i]<< endl;

    // Il seguente ciclo serve per criptare il testo di partenza
    for(i=0;i<lenText;i++)
    {
        asciiEn[i]=v[i] + asciiP[i%size];
        cout<< "result:"<< asciiEn[i]<< endl;
    }

    cout<< lenText<< endl;

    // Con il seguente ciclo viene riconvertito il testo criptato dal codice ascii a caratteri in formato char

    char c;
    c = char(asciiEn[2]);
    cout<< c<< endl;
    for(i=0;i<(lenText);i++)
    {
        encryptedText = encryptedText + char(asciiEn[i]);
    }
    cout<< encryptedText << endl;

    return true;

}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    int i=0,size=0,lenText=0;

    while(password[size]!='\0')
        size++;

    while(text[lenText]!='\0')
        lenText++;

    cout<< password<< endl;
    cout<< size<< endl;

    // In "asciiP" viene salvata la password convertita in codice ascii, in "v" il testo convertito in ascii, e in "asciiEn" il testo decriptato, in codice ascii.
    int asciiP[size+1],v[lenText],asciiEn[lenText];

    // Il seguente ciclo for serve per convertire la password in codice ascii
    for(i=0;i<size;i++)
        asciiP[i]=int(password[i]);
    for(i=0;i<size;i++)
        cout<< asciiP[i]<< endl;


    // Il seguente ciclo for serve per convertire il testo da decriptare in codice ascii
    for(i=0;i<lenText;i++)
        v[i]=int(text[i]);

    for(i=0;i<lenText;i++)
        cout<< v[i]<< endl;

    // Il seguente ciclo serve per decriptare il testo di partenza
    for(i=0;i<lenText;i++)
    {
        asciiEn[i]=v[i] - asciiP[i%size];
        cout<< "result:"<< asciiEn[i]<< endl;
    }

    cout<< lenText<< endl;


    // Con il seguente ciclo viene riconvertito il testo decriptato, dal codice ascii a caratteri in formato char
    for(i=0;i<(lenText);i++)
    {
        decryptedText[i]=char(asciiEn[i]);
        //cout<< decryptedText[i];
    }

    cout<< decryptedText<< endl;


    return true;
}
