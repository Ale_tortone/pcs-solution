#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoInteresection;
    intersectionParametricCoordinate = 0.0;
}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************

void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   planeTranslationPointer = &planeTranslation; //corrisponde a 'd' nell'equazione generica del piano ed è ottenuto dal prodotto scalare tra n e x0.
   planeNormalPointer = &planeNormal; // n, la normale al piano

   return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    // nell'equazione generica della retta y= y0+s*t:
    lineOriginPointer = &lineOrigin; // corrisponde a y0
    lineTangentPointer = &lineTangent; // corrisponde a t, vettore tangente

    return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    // parte da togliere:
    int i;
    float prod_scal=0;
    for (i=0;i<3;i++)
    {
       prod_scal = prod_scal + ( (*lineTangentPointer)(i) * (*planeNormalPointer)(i) );
    }
    // Fino a qua


    // Affinchè una retta e un piano possano avere un solo punto di intersezione, il prodotto scalare tra la normale al piano 'n' e
    // il vettore tangente 't' della retta deve essere diverso da zero, cioè il caso in cui i due vettori siano non perpendicolari
    // Nell'argomento dell'if viene calcolato tale prodotto scalare

    if ( ((*lineTangentPointer)(0)*(*planeNormalPointer)(0) + (*lineTangentPointer)(1)*(*planeNormalPointer)(1) + (*lineTangentPointer)(2)*(*planeNormalPointer)(2) ) > toleranceParallelism)
    {
       intersectionParametricCoordinate = ( (*planeTranslationPointer)- ( (*planeNormalPointer)(0)*(*lineOriginPointer)(0) + (*planeNormalPointer)(1)*(*lineOriginPointer)(1) + (*planeNormalPointer)(2)*(*lineOriginPointer)(2) ) ) / ( (*lineTangentPointer)(0)*(*planeNormalPointer)(0) + (*lineTangentPointer)(1)*(*planeNormalPointer)(1) + (*lineTangentPointer)(2)*(*planeNormalPointer)(2) );
       intersectionType = PointIntersection;
       return true;
    }

    // Nel caso i cui i due vettori siano perpendicolari, possiamo avere due casi:
    //1) La retta appartiene al piano (complanari)
    //2) La retta appartiene a un piano parallelo a quello dato (non si hanno quindi punti di intersezione tra i due)

    else
    {
        //1) Perchè la retta sia complanare al piano è necessario che n*(x0-y0) sia uguale a zero, cioè d-n*y0=0 :

        double numeratore=0;
        numeratore = (*planeTranslationPointer)- ( (*planeNormalPointer)(0)*(*lineOriginPointer)(0) + (*planeNormalPointer)(1)*(*lineOriginPointer)(1) + (*planeNormalPointer)(2)*(*lineOriginPointer)(2) );

        //In questo caso bisogna considerare un range di soluzioni ammissibili, dovuto alla precisione finita di calcolo (|numeratore|<tolleranza)

        if( numeratore < toleranceIntersection && numeratore> (-toleranceIntersection))
        {
            intersectionType = Coplanar;
        }

        //2) Ultimo caso di retta parallela al piano e nessun  punto di intersezione:

        else
        {
            intersectionType = NoInteresection;
        }

        return false;
    }
}













