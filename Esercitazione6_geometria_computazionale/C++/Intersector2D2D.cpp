#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D2D::NoInteresection;
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    //L'equazione generica del piano è esprimibile con: N*X-N*p0=0, oppure N*X-d=0
    matrixNomalVector.row(0)=planeNormal; // vettore normale al primo piano, N1
    rightHandSide(0)= planeTranslation; // corrisponde alla 'd1' dell'equazione generica del piano, con d=N1*p1

  return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{

    matrixNomalVector.row(1)=planeNormal; // vettore normale al secondo piano, N2
    rightHandSide(1)= planeTranslation; // corrisponde alla 'd2', con d2=N2*p2


  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    double check,moduloT=0;

    // Il vettore tangente alla retta ottenuta dall'intersezione dei due piani è dato dal prodotto vettoriale delle normali dei due piani.

    tangentLine(0) = matrixNomalVector(0,1)*matrixNomalVector(1,2) - matrixNomalVector(0,2)*matrixNomalVector(1,1);
    tangentLine(1) = matrixNomalVector(0,2)*matrixNomalVector(1,0) - matrixNomalVector(0,0)*matrixNomalVector(1,2);
    tangentLine(2) = matrixNomalVector(0,0)*matrixNomalVector(1,1) - matrixNomalVector(0,1)*matrixNomalVector(1,0);

    matrixNomalVector.row(2)= TangentLine();

    // Controllo sulla condizione di parallelismo tra le due normali ai piani: ||t^2|| < toll^2*||N1||*||N2||
    // (al posto del modulo di t al quadrato si sarebbe potuto fare lo stesso conto con il determinante della matrice dei vettori normali,
    // in quanto il determinate coincide con il prodotto misto tra i tre vettori)

    check = toleranceParallelism * toleranceParallelism * matrixNomalVector.row(0).squaredNorm() *  matrixNomalVector.row(1).squaredNorm();
    moduloT = tangentLine.squaredNorm(); // qui viene salvato il modulo della tangente alla retta al quadrato


    if( moduloT < check && moduloT > (-check))
    {
        //In questo caso i due vettori risultano essere paralleli e ci si ritrova in due possibili casi:
        //1)I due piani sono complanari, se  N1P1=N2P2, cioè se |d1 - d2|< toll
        //2)I due piani sono paralleli, in caso contrario.

        if(rightHandSide(0)-rightHandSide(1) < toleranceIntersection && rightHandSide(0)-rightHandSide(1) > -toleranceIntersection)
            intersectionType = Coplanar; //caso 1)
        else
            intersectionType= NoInteresection; //caso 2)

        return false;
    }


    else
    {
        // In questo caso i due vettori non sono paraelli e l'intersezione dei due piani e una retta di vettore tangente t (trovato precedentemente)
        intersectionType = LineIntersection;
        return true;
    }


}
