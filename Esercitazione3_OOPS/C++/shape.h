#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <cmath>

using namespace std;

namespace ShapeLibrary {

  class Point {

    public:
      double _x;
      double _y;
    public:
      Point(const double& x =0,
            const double& y =0);
      Point(const Point& point) {_x = point._x; _y = point._y ;}
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
     protected:
      double _a;
      double _b;
      Point _center;
     public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };

  class Triangle : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;


    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public Parallelogram
  {
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
