#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;

}



Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
   _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Area() const { return M_PI * _a *_b; }

// Il cerchio sfrutta l'implementazione dell'area dell'ellisse, ma in cui a = b = raggio
Circle::Circle(const Point &center, const int &radius) : Ellipse (center,radius,radius)
{
}

double Circle::Area() const { return Ellipse ::Area(); }



Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
}

double Triangle::Area() const
{
    //Formula di Gauss per il calcolo dell'area di poligoni
    return  0.5* abs(_p1._x*_p2._y + _p2._x*_p3._y + _p3._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p1._x*_p3._y );
}

// Uso l'implementazione del triangolo per l'area di quello equilatero, sapendo però le coordinate di un solo punto e la lunghezza del lato
TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle ( p1, Point((p1._x + 0.5*edge),(p1._y + sqrt(3)/2*edge)), Point( (p1._x-0.5*edge) , (p1._y + sqrt(3)/2*edge)) )
{
}

double TriangleEquilateral::Area() const { return Triangle :: Area(); }



Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
   _p1 =p1;
   _p2 = p2;
   _p3 = p3;
   _p4 = p4;
}

double Quadrilateral::Area() const
{
    return  0.5* abs(_p1._x*_p2._y + _p2._x*_p3._y + _p3._x*_p4._y + _p4._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p4._x*_p3._y - _p1._x*_p4._y);
}

//Stessa area del quadrilatero ma si sfrutta il fatto che i lati opposti abbiano la medesima lunghezza
Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral (p1,p2, Point( (p4._x + abs(p2._x-p1._x)) , p4._y) , p4)
{
}

double Parallelogram::Area() const { return Quadrilateral ::Area(); }

//Stessa area del parallelogramma, ma si trovano le coordinate degli altri due punti conoscendo base e altezza
Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram ( p1, Point( (p1._x+base), p1._y) , Point(p1._x, (p1._y+height) ))
{
}

double Rectangle::Area() const { return Parallelogram ::Area(); }

//Stessa area del rettangolo ma con base = altezza = lato
Square::Square(const Point &p1, const int &edge) : Rectangle (p1,edge,edge)
{
}

double Square::Area() const{ return Rectangle ::Area(); }


}
