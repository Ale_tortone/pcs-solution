import math

class Point:
    def __init__(self, x: float, y: float):
        self._x = x
        self._y = y
        pass


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b
    def area(self):
        return self.a * self.b * math.pi
        pass


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        self.radius = radius
        self.center = center
    def area(self):
        #Stessa area dell'ellisse con a = b = raggio
        return Ellipse.area(Ellipse(self.center,self.radius,self.radius) )
        pass


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
    def area(self):
        # Si fa uso, per il calcolo dell'area, della formula di Gauss, che permette di trovare l'area di un qualsiasi poligono date le coordinate dei suoi punti
        return 0.5* abs(self.p1._x*self.p2._y + self.p2._x*self.p3._y + self.p3._x*self.p1._y - self.p2._x*self.p1._y - self.p3._x*self.p2._y - self.p1._x*self.p3._y )
        pass


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge

    def area(self):
        # Stessa area del triangolo scaleno, ma sfruttando il fatto che le coordinate degli altri due punti siano univocamente individuate da un punto e dalla lunghezza del lato
        return Triangle.area(Triangle ( self.p1 ,  Point (self.p1._x + 0.5 * self.edge , self.p1._y + math.sqrt(3) / 2 * self.edge) , Point (self.p1._x - 0.5 * self.edge , self.p1._y + math.sqrt(3) / 2 * self.edge) ) )
        pass

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4
    def area(self):
        # Anche qui si fa uso della formula di Gauss per il calcolo dell'area
        return 0.5* abs(self.p1._x*self.p2._y + self.p2._x*self.p3._y + self.p3._x*self.p4._y + self.p4._x*self.p1._y - self.p2._x*self.p1._y - self.p3._x*self.p2._y - self.p4._x*self.p3._y - self.p1._x*self.p4._y)
        pass


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p4 = p4

    def area(self):
        # Stessa area del quadrilatero qualsiasi, ma si sfrutta il fatto che le lunghezze dei lati opposti paralleli siano uguali
        return Quadrilateral.area(Quadrilateral ( self.p1 , self.p2 , Point ( self.p4._x + abs (self.p2._x - self.p1._x)  , self.p4._y ) , self.p4 ) )
        pass


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1 = p1
        self.base = base
        self.height = height

    def area(self):
        # Stessa area del parallelogramma, ma si ottengono le coordinate degli altri due punti grazie al fatto che si conoscono base e altezza
        return Parallelogram.area( Parallelogram ( self.p1, Point( self.p1._x+self.base, self.p1._y) , Point( self.p1._x, self.p1._y+self.height)))
        pass


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge
    def area(self):
        # Stessa area del rettangolo ma con base = altezza = lato
        return Rectangle.area (Rectangle ( self.p1, self.edge, self.edge))
        pass
